<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 21/05/14
 * Time: 20:39
 */

use Illuminate\Console\Command;

/**
 * Class EmagineersTasksAbstract
 *
 * @abstract
 *
 * This class is used to provide common tools and utilities for the command
 * line Tasks.
 */
abstract class EmagineersTasksAbstract extends Command{

    /**
     * Fetches the STDIN input stream.
     *
     * @access protected
     * @return string
     */
    protected function stdin(){
        // read from stdin
        $fd = fopen("php://stdin", "r");
        $rawInput = "";
        while (!feof($fd)) {
            $rawInput .= fread($fd, 1024);
        }
        fclose($fd);

        return $rawInput;
    }
} 