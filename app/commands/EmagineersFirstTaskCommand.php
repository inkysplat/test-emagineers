<?php
/**
 * Load in the Abstract Class if it isn't found
 */
if(!class_exists('EmagineersTasksAbstract'))
    require __DIR__.DIRECTORY_SEPARATOR.'EmagineersTasksAbstract.php';

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class EmagineersFirstTaskCommand extends EmagineersTasksAbstract {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'emagineers:first-task';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'First Task for Emagineers Coding Test';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$stdin = $this->stdin();

        $json = json_decode($stdin, true);

        if(!$json){
            throw new Exception(__METHOD__."::Error decoding JSON");
        }

        $order = new Order();
        $order->hydrate($json);

        echo json_encode($order->getOrder());
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
