<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 21/05/14
 * Time: 21:02
 */

class DiscountSeeder extends Seeder {

    public function run()
    {
        DB::table('discount')->delete();

        Discount::create(
            array('coupon' => 'percentage_off_5','percent' => 5.00)
        );

        Discount::create(
            array('coupon' => 'percentage_off_10','percent' => 10.00)
        );

        Discount::create(
            array('coupon' => 'percentage_off_20','percent' => 2.50)
        );

        Discount::create(
            array('coupon' => 'money_off_5','discount' => 5.00)
        );

        Discount::create(
            array('coupon' => 'money_off_10','discount' => 10.00)
        );

        Discount::create(
            array('coupon' => 'money_off_20','discount' => 2.50)
        );

        Discount::create(
            array('coupon' => 'spend_x_5','discount' => 1.49,'threshold'=>5.00)
        );

        Discount::create(
            array('coupon' => 'spend_x_25','discount' => 4.50,'threshold'=>25.00)
        );

        Discount::create(
            array('coupon' => 'spend_x_33','discount' => 20.00,'threshold'=>33.00)
        );

        Discount::create(
            array('coupon' => 'FREESHIPPING')
        );
    }

}