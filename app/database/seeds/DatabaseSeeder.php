<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
        $this->call('ShippingSeeder');
        $this->call('DiscountSeeder');
		// $this->call('UserTableSeeder');
	}

}
