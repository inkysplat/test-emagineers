<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 21/05/14
 * Time: 21:02
 */

class ShippingSeeder extends Seeder {

    public function run()
    {
        DB::table('shipping')->delete();

        Shipping::create(
            array('country' => 'UK','fee' => 2.50,'free_qualifier'=>10.00)
        );
        Shipping::create(
            array('country' => 'DE', 'fee' => 5.00,'free_qualifier'=>15.00)
        );
        Shipping::create(
            array('country' => 'IS', 'fee' => 6.00,'free_qualifier'=>17.50)
        );
        Shipping::create(
            array('country' => 'AU', 'fee'=>10.00,'free_qualifier'=>30.00)
        );
        Shipping::create(
            array('country' => 'US', 'fee'=>10.00,'free_qualifier'=>30.00)
        );
        Shipping::create(
            array('country' => 'AE', 'fee'=>10.00,'free_qualifier'=>30.00)
        );
    }

}