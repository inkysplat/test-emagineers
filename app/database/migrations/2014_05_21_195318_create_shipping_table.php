<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shipping', function(Blueprint $table)
		{
			$table->string('country',2);
            $table->decimal('fee',5,2);
            $table->decimal('free_qualifier',5,2);
            $table->timestamps();
            $table->unique('country');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('shipping');
	}

}
