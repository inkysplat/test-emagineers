<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 21/05/14
 * Time: 21:37
 */

class ShippingTest  extends TestCase {

    /**
     * Contains Fixtures for each test
     * @var array
     */
    private $mocks = array(
        'testShippingCountry' => array(
            'order' => array(
                'subtotal' => 0.00,
                'grand_total' => 0.00,
                'shipping_total' => null,
                'shipping_country' => 'AZ'
            )
        ),
        'testUKShipping' => array(
            'order' => array(
                'subtotal' => 0.00,
                'grand_total' => 0.00,
                'shipping_total' => null,
                'shipping_country' => 'UK'
            )
        ),
        'testNonUKShipping' => array(
            'order' => array(
                'subtotal' => 0.00,
                'grand_total' => 0.00,
                'shipping_total' => null,
                'shipping_country' => 'US'
            )
        ),
        'testUKFreeShipping' => array(
            'order' => array(
                'subtotal' => 10.00,
                'grand_total' => 10.00,
                'shipping_total' => null,
                'shipping_country' => 'UK'
            )
        ),
        'testNonUKFreeShipping' => array(
            'order' => array(
                'subtotal' => 30.00,
                'grand_total' => 35.00,
                'shipping_total' => null,
                'shipping_country' => 'US'
            )
        ),
        'testUKNonFreeShipping' => array(
            'order' => array(
                'subtotal' => 5.00,
                'grand_total' => 5.00,
                'shipping_total' => null,
                'shipping_country' => 'UK'
            )
        ),
    );

    /**
     * Stores the Order Object
     * @var Order
     */
    private $order;

    /**
     * Setups the test
     */
    public function setUp(){

        parent::setUp();
        $this->prepareForTests();
    }

    /**
     * Tests that a shipping country is valid
     */
    public function testShippingCountry(){

        try{
            $order = new Order();
            $order->hydrate($this->mocks[__FUNCTION__]['order']);
            $this->fail("Expected Invalid Shipping Country Exception");
        }catch(InvalidCountryException $e){

        }
    }

    /**
     * Tests UK Shipping Rates
     */
    public function testUKShipping(){

        $order = new Order();
        $order->hydrate($this->mocks[__FUNCTION__]['order']);
        $shippingTotal = $order->getShippingTotal();

        $this->assertEquals($shippingTotal, 2.50);
    }


    /**
     * Tests non-UK Shipping Rate.
     */
    public function testNonUKShipping(){

        $order = new Order();
        $order->hydrate($this->mocks[__FUNCTION__]['order']);
        $shippingTotal = $order->getShippingTotal();

        $this->assertNotEquals($shippingTotal, 2.50);
    }

    /**
     * Tests if an order qualifies for free shipping.
     */
    public function testUKFreeShipping(){

        $order = new Order();
        $order->hydrate($this->mocks[__FUNCTION__]['order']);
        $shippingTotal = $order->getShippingTotal();

        $this->assertEquals($shippingTotal, 0.00);
    }

    /**
     * Tests if an order qualifies for free shipping.
     */
    public function testNonUKFreeShipping(){

        $order = new Order();
        $order->hydrate($this->mocks[__FUNCTION__]['order']);
        $shippingTotal = $order->getShippingTotal();

        $this->assertEquals($shippingTotal, 0.00);
    }

    /**
     * Tests if an order doesn't qualifies for free shipping.
     */
    public function testUKNonFreeShipping(){

        $order = new Order();
        $order->hydrate($this->mocks[__FUNCTION__]['order']);
        $shippingTotal = $order->getShippingTotal();

        $this->assertNotEquals($shippingTotal, 0.00);
    }
} 