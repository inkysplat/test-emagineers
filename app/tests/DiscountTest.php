<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 21/05/14
 * Time: 23:15
 */

class DiscountTest  extends TestCase {

    /**
     * Contains Fixtures for each test
     * @var array
     */
    private $mocks = array(
        'testCouponCode' => array(
            'order' => array(
                'subtotal' => 9.00,
                'grand_total' => 9.00,
                'coupon_code' => "dsfhdsf",
                'shipping_total' => null,
                'shipping_country' => 'UK'
            )
        ),
        'testFreeShipping' => array(
            'order' => array(
                'subtotal' => 9.00,
                'grand_total' => 9.00,
                'coupon_code' => "FREESHIPPING",
                'shipping_total' => null,
                'shipping_country' => 'UK'
            )
        )
    );

    /**
     * Stores the Order Object
     * @var Order
     */
    private $order;

    /**
     * Setups the test
     */
    public function setUp(){

        parent::setUp();
        $this->prepareForTests();
    }

    /**
     * Test Coupon is working
     */
    public function testCouponCode(){

        try{
            $order = new Order();
            $order->hydrate($this->mocks[__FUNCTION__]['order']);
            $order->getDiscountAmount();

            $this->fail("Expecting Invalid Coupon");
        }catch(InvalidCouponException $e){

        }
    }

    /**
     *
     */
    public function testFreeShipping(){

        $order = new Order();
        $order->hydrate($this->mocks[__FUNCTION__]['order']);

        $this->assertEquals($order->getShippingTotal(), 2.50);
        $this->assertEquals($order->getShippingTotal(), $order->getDiscountAmount());
    }

} 