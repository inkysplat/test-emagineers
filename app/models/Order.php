<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 21/05/14
 * Time: 21:22
 */

/**
 * Class Order
 *
 * This class is used to handle orders and provide
 * shipping and discount costs.
 *
 * It is separate from the EmgineerTaskCommands as this
 * class could also be accessed via a web interface.
 *
 * The Hydrate method is a way of importing the order
 * into the scope of this class to then calculate
 * it's discounts and shippings fees.
 *
 * For the purposes of the Emagineer's test
 * we'll be hydrating using a JSON feed,
 * however in reality the order could come from
 * session data or a database.
 */
class Order {

    /**
     * @access private
     * @var float
     */
    private $_subTotal;

    /**
     * @access private
     * @var float
     */
    private $_grandTotal;

    /**
     * @access private
     * @var float
     */
    private $_shippingTotal;

    /**
     * @access private
     * @var string
     */
    private $_shippingCountry;

    /**
     * @access private
     * @var string
     */
    private $_discountCoupon;

    /**
     * @var float
     */
    private $_discountAmount;

    /**
     * An Array of Shipping Countries that
     * we allow shipping to.
     * @var array
     */
    private $_shippingCountries = array();

    /**
     * Stores the Shipping data in here.
     * @var array
     */
    private $_shipping = array();

    /**
     * @var array
     */
    private $_coupons = array();

    /**
     * @var array
     */
    private $_couponCodes = array();

    /**
     * @return
     */
    public function __construct(){

        $countries = Shipping::all();
        foreach($countries as $country){
            $this->_shippingCountries[]         = $country->country;
            $this->_shipping[$country->country] = $country;
        }

        $coupons = Discount::all();
        foreach($coupons as $coupon){
            $this->_coupons[$coupon->coupon]    = $coupon;
            $this->_couponCodes[]               = $coupon->coupon;
        }
    }

    /**
     * @param String $country
     */
    public function setShippingCountry($country){

        if(!in_array($country, $this->_shippingCountries)){
            throw new InvalidCountryException();
        }

        $this->_shippingCountry = $country;
    }

    /**
     * @return string
     */
    public function getShippingCountry(){
        return $this->_shippingCountry;
    }

    /**
     * @param $shippingTotal
     */
    public function setShippingTotal($shippingTotal){
        $this->_shippingTotal = $shippingTotal;
    }

    /**
     * @param $country
     * @return mixed
     */
    private function _getShipping($country){
        if(!empty($this->_shipping[$country])){
            return $this->_shipping[$country];
        }
        return Shipping::where('country','=',$country)->first();
    }

    /**
     * Calculates Shipping Costs based on
     * @return mixed
     */
    public function getShippingTotal(){
        if(empty($this->_shippingTotal)){

            $country = $this->getShippingCountry();
            $shipping = $this->_getShipping($country);

            if($this->getSubTotal() >= $shipping->free_qualifier){
                $this->setShippingTotal(0.00);
            }else{
                $this->setShippingTotal($shipping->fee);
            }
        }

        return $this->_shippingTotal;
    }

    /**
     * @param $subtotal
     */
    public function setSubTotal($subtotal){
        $this->_subTotal = $subtotal;
    }

    /**
     * @return float
     */
    public function getSubTotal(){
        return $this->_subTotal;
    }

    /**
     * @param $grandtotal
     */
    public function setGrandTotal($grandtotal){
        $this->_grandTotal = $grandtotal;
    }

    /**
     * @return float
     */
    public function getGrandTotal(){
        return $this->_grandTotal;
    }

    /**
     * @param $coupon
     */
    public function setCouponCode($coupon){

        if(!in_array($coupon, $this->_couponCodes)){
            throw new InvalidCouponException(__METHOD__."::Invalid Coupon Code");
        }

        $this->_discountCoupon = $coupon;
    }

    /**
     * @return string
     */
    public function getCouponCode(){
        return $this->_discountCoupon;
    }

    /**
     * @param $amount
     */
    public function setDiscountAmount($amount){
        $this->_discountAmount = $amount;
    }

    /**
     *
     */
    public function getDiscountAmount(){
        if(empty($this->_discountAmount)){
            $coupon = $this->getCouponCode();
            $discount = $this->_getDiscount($coupon);
            if($discount){
                $accessor = $this->_getDiscountAccessor($coupon);
                $model = new Discount();
                if(method_exists($this, $accessor)){
                    $amount = $model->$accessor($this);
                    $this->setDiscountAmount($amount);
                    return $this->_discountAmount;
                }
            }
        }

        return $this->_discountAmount;
    }

    /**
     * @param $coupon
     * @return string
     */
    private function _getDiscountAccessor($coupon){
        return "process".ucfirst(substr($coupon,0,strpos($coupon,'_')));
    }

    /**
     * @param $coupon
     * @return mixed
     */
    private function _getDiscount($coupon){
        if(isset($this->_coupons[$coupon]))
            return $this->_coupons[$coupon];
        return false;
    }

    /**
     * @access public
     * @param array $order
     * @return $this
     */
    public function hydrate(Array $order){

        foreach($order as $key=>$value){
            switch(strtolower($key)){
                case 'subtotal':
                    $this->setSubTotal($value);
                    break;
                case 'grand_total':
                    $this->setGrandTotal($value);
                    break;
                case 'shipping_total':
                    $this->setShippingTotal($value);
                    break;
                case 'shipping_country':
                    $this->setShippingCountry($value);
                    break;
                case 'coupon_code':
                    $this->setCouponCode($value);
                    break;
            }
        }

        return $this;
    }

    /**
     * Returns the Order as an Array
     * @return array
     */
    public function getOrder(){
        return array(
            'subtotal'          => $this->getSubTotal(),
            'grand_total'       => $this->getGrandTotal(),
            'discount_amont'    => $this->getDiscountAmount(),
            'coupon_code'       => $this->getCouponCode(),
            'shipping_total'    => $this->getShippingTotal(),
            'shipping_country'  => $this->getShippingCountry()
        );
    }
} 