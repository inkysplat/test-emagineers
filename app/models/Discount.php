<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 21/05/14
 * Time: 21:02
 */

class Discount  extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'discount';

    /***
     * @access public
     * @param Order $order
     * @return mixed
     */
    public function processFreeshipping(Order $order){
        return $order->getShippingTotal();
    }

    /**
     * @param Order $order
     */
    public function processPercentage(Order $order){

    }

    /**
     * @param Order $order
     */
    public function processMoney(Order $order){

    }

    /**
     * @param Order $order
     */
    public function processSpend(Order $order){

    }
} 